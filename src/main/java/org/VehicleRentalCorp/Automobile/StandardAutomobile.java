package org.VehicleRentalCorp.Automobile;

import java.util.Date;

public class StandardAutomobile extends Automobile {

	private Date lastEmissionTestDate;
	private int numberOfSeats;

	private int cost;

	public StandardAutomobile(int vin, int vehicleRange) {
		super(vin, vehicleRange);
		this.basePrice = 3000;
	}

	public Date getLastEmissionTestDate() {
		return lastEmissionTestDate;
	}

	public void setLastEmissionTestDate(Date lastEmissionTestDate) {
		this.lastEmissionTestDate = lastEmissionTestDate;
	}

	public int getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(int numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

	public void displayInfo() {

		cost = fareCalculater();

		/*
		 * System.out.println(CarType.STANDARD.getValue() + " by " +
		 * getManufacturername() + " is available to rent in " + this.getColor() +
		 * ".This beast has a range of " + this.vehicleRange + " and only costs $" +
		 * cost);
		 */
		// System.out.printf("%nREsult : %d / %d = %d%n" , a , b ,c );
	}

	public int fareCalculater() {

		int cost = 0;

		return cost;
		// TODO Auto-generated method stub

	}

	public boolean isLimitable() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isRenewable() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean checkEmissionTest() {

		return false;

	}

}
