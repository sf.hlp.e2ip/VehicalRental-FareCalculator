package org.VehicleRentalCorp.Automobile;

import org.VehicleRentalCorp.Automobile.Exception.OutOfRangeException;

public class VehicleSelection {

	private boolean isLimitableRule = false;
	DieselSemitruckAutomobile semitruck;

	public String automobileSelectionCriteria(String vehicleType) {

		try {
			if ("Semitruck".equals(vehicleType)) {
				boolean islimitableRule = semiTruckValidation(vehicleType, 100);
				semitruck.displayInfo(islimitableRule, vehicleType);
			}

		} catch (Exception e) {
			e.getMessage();
		}
		return vehicleType;
	}

	public boolean semiTruckValidation(String vehicleType, int range) throws OutOfRangeException {

		if (range > 60 && range < 150) {
			isLimitableRule = true;

		}
		return isLimitableRule;

	}

}

