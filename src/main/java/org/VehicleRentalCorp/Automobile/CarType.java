package org.VehicleRentalCorp.Automobile;

public enum CarType {

	STANDARD("standard"), ELECTRIC("electric"), DIESEL("diesel");

	String value;

	public String getValue() {
		return value;

	}

	private CarType(String Value) {
		this.value = Value;
	}
}
