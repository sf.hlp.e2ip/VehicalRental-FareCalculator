package org.VehicleRentalCorp.Automobile;


public abstract class Automobile implements Vehicle {

	protected int vin;
	protected int vehicleRange;
	protected int basePrice;
	protected int vehicleCode;
	private String color = "Black";
	private final String manufacturerName = "XYZ";


	public Automobile(int vin, int vehicleRange) {
		this.vin = vin;
		this.vehicleRange = vehicleRange;
	}

	public int getBasePrice() {
		return basePrice;
	}

	public abstract void displayInfo();

	public abstract int fareCalculater();

	public String getManufacturername() {
		return manufacturerName;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
