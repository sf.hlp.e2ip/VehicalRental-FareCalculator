package org.VehicleRentalCorp.Automobile.Exception;

public class OutOfRangeException extends Exception {


	private static final long serialVersionUID = 1L;

	public OutOfRangeException(IllegalArgumentException e) {
		super(e);
	}

}