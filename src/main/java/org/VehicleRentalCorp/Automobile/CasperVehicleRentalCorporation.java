package org.VehicleRentalCorp.Automobile;

import java.util.Scanner;

import org.VehicleRentalCorp.Boat.Boat;

public class CasperVehicleRentalCorporation {
	
	static ElectricAutomobile electricCar1;
	static ElectricAutomobile electricCar2;
	public static DieselSemitruckAutomobile semiTruck;
	static DieselAutomobile dieselCar;
	static StandardAutomobile standardCar;
	static VehicleSelection vehicleSelection = new VehicleSelection();
	static Boat boat;
	static Scanner input;
	static String vehicleType;

	public static void main(String[] args) {
		
		input = new Scanner(System.in);

		System.out.println("Enter Vehicle type to get More Info\n");

		inputVehicleType();
		
		/*electricCar1 = new ElectricAutomobile(1234, 80, 30000, "Black");
		electricCar2 = new ElectricAutomobile(1234, 80, 30000, "Black");
		semiTruck = new DieselSemitruckAutomobile(3456, 50, 20000, "White");
		dieselCar = new DieselAutomobile(6789, 200, 25000, "Gray");
		standardCar = new StandardAutomobile(234, 300, 35000, "Navy Blue");
		boat = new Boat(2343, 344, 100000, "White-Blue");
		
		// To Call all Display information for car Vehicles - Electric , Semi Truck , Diesel , Standard
		System.out.printf(" \n\t\t\t\t  Car Information       \n\n");
		
		electricCar1.displayInfo();
		electricCar2.displayInfo();
		semiTruck.displayInfo();
		dieselCar.displayInfo();
		standardCar.displayInfo();
		boat.displayInfo();*/
	}

	private static void inputVehicleType() {

		boolean repeat = true;
		while(repeat ) {
			
			vehicleType = input.next();	
			System.out.println("Vehicle type" + vehicleType);
			System.out.println("Vehicle selection " + vehicleSelection );
			vehicleType = vehicleSelection.automobileSelectionCriteria(vehicleType);		
			System.out.println("Do you want to continue? (Type true/false)\n");
			repeat = input.nextBoolean();
			
		}
	}

}
