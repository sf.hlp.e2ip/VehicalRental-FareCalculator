package org.VehicleRentalCorp.Boat;

import org.VehicleRentalCorp.Automobile.Automobile;

public class Boat extends Automobile {

	Boat(int vin, int vehicleRange) {
		super(vin, vehicleRange);

	}

	private int cost;

	public void displayInfo() {

		cost = fareCalculater();

		System.out.println(BoatType.SPEED.getValue() + " with VIN " + this.vin + " is available to rent."
				+ " This beauty has a range of " + this.vehicleRange + " and only costs $" + cost);

	}

	@Override
	public int fareCalculater() {

		int cost = 0;

		return cost;
		// TODO Auto-generated method stub

	}

	public boolean isLimitable() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isRenewable() {
		// TODO Auto-generated method stub
		return false;
	}

}
