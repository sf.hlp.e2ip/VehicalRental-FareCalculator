package org.VehicleRentalCorp.Boat;

public enum BoatType {

	BARGE("barge"), CARGO("cargo"), SPEED("speed"), YACHT("yacht");

	private String value;

	private BoatType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
